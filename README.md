# Very Simple Markup Language

vsml is a library inspired by HAML, but without #id .class and templating features, as it is intended solely for
defining structure and not to be rendered to HTML directly.

## Syntax

<tag>(=<value>)?(<key>(=<value>)?)*

### Tags

The only building block of vsml document is *tag*.
Every line defines a tag (unless it has backslash at the end).
Tags may have (primary) *values*, attributes and children tags.
They don't have bodies.
Tags are nested by *indentation*.

### 'root' tag

'root' is a special tag denoting start of document. It can be omitted (like in example below), but will always be
generated.

### Attributes

Each tag may have zero or more attributes separated by semicolon. Attributes is key-value map.

### Indentation

Though it is possible to use any number of spaces to indent block it is preferable to use a multiple of four

### Escaping

';', '=' and EOL symbols can be escaped by prepending with a backslash

### Example

    cols=2;title=One
        ap=123;width=1
            ap=456;tricky = Space \
            Separated \
            Chaos\;\
            Head
        ap=789;a=1000;title=Steins\;Gate
    cols=3
        group=Variable
            ap=135;tricky \= key = Tricky \= value
            ap=246
        empty tag
        And, yeah, spaces = allowed