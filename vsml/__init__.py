# coding=utf-8
"""
Very Simple Markup Language
"""

# Copyright (c) 2014 Mikhael Malkov
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os

__author__ = 'viruzzz-kun'
__version__ = '0.1'


class VsmlException(Exception):
    pass


class VsmlElement(object):
    def __init__(self, tag, value, attrs):
        """ Constructor
        @param tag: tag name
        @param value: primary value of tag
        @param attrs: attributes dict
        @type tag: str | unicode
        @type value: str | unicode | int | float | NoneType | bool
        @type attrs: dict
        """
        self.tag = tag
        self.value = value
        self.attrs = attrs
        self.children = []
        self.parent = None

    def add_child(self, elem):
        """
        @param elem: appending element
        @type elem: VsmlElement
        """
        if elem.parent and elem in elem.parent.children:
            elem.parent.children.remove(elem)
        self.children.append(elem)
        elem.parent = self

    @classmethod
    def from_string(cls, string):
        """ Parse string to form element tree
        @param string: source string
        @type string: str | unicode
        @rtype VsmlElement
        """
        substr1 = ''
        while 1:
            substr1 = os.urandom(4).encode('hex')
            if not substr1 in string:
                break
        substr2 = ''
        while 1:
            substr2 = os.urandom(4).encode('hex')
            if not substr2 in string and substr1 != substr2:
                break
        string = string.replace('\;', substr1).replace('\=', substr2)

        root = VsmlElement('root', None, {})
        path = [(root, -1)]
        parent = root
        prev = root
        prev_indent = 0
        lines_iter = iter(string.splitlines())
        for line in lines_iter:
            line = line.rstrip()
            while line.endswith('\\'):
                line = line[:-1] + lines_iter.next().strip()
            unindented = line.lstrip()
            if not unindented:
                continue
            indent = len(line) - len(unindented)

            parts = [part.replace(substr1, ';') for part in unindented.split(';')]

            tag_t = [part.replace(substr2, '=') for part in parts[0].split('=', 1)]
            tag = tag_t[0].strip()
            value = tag_t[1].strip() if len(tag_t) == 2 else None
            attrs = {}

            for a in parts[1:]:
                attr_t = a.split('=', 1)
                attrs[attr_t[0].strip().replace(substr2, '=')] = attr_t[1].strip().replace(substr2, '=') if len(attr_t) == 2 else None

            ctag = VsmlElement(tag, value, attrs)

            if tag == 'root':
                # root tag restarts everything
                root = ctag
                parent = root
                prev = root
                prev_indent = indent
                path = [(root, indent)]
                continue

            if indent > prev_indent:
                parent = prev
                path.append((parent, prev_indent))
            elif indent < prev_indent:
                while prev_indent >= indent:
                    if not path:
                        return root
                    parent, prev_indent = path.pop()
            parent.add_child(ctag)
            prev = ctag
            prev_indent = indent
        return root

    def as_string(self, indent):
        """ Format as unicode string
        @param indent: indentation level
        @type indent: int
        @rtype unicode
        """
        result = [u'    ' * indent]
        if self.value is not None:
            result.append(
                u'%s=%s' % (
                    unicode(self.tag).replace(u';', u'\;').replace('=', '\='),
                    unicode(self.value).replace(u';', u'\;').replace('=', '\=')
                )
            )
        else:
            result.append(self.tag.replace(u';', u'\;').replace('=', '\='))
        for key, value in self.attrs.iteritems():
            result.append(u';')
            result.append(
                u'%s=%s' % (
                    unicode(key).replace(u';', u'\;').replace('=', '\='),
                    unicode(value).replace(u';', u'\;').replace('=', '\='))
                if value not in (True, None)
                else unicode(key).replace(u';', u'\;').replace('=', '\='))
        result.append(u'\n')
        result.extend(elem.as_string(indent + 1) for elem in self.children)
        return u''.join(result)

    def as_dict(self):
        return {
            'tag': self.tag,
            'value': self.value,
            'attrs': self.attrs,
            'children': [child.as_dict() for child in self.children]
        }

    def __unicode__(self):
        return self.as_string(0)

if __name__ == "__main__":
    from pprint import pprint
    a = u"""
cols=2;title=One
    ap=123;width=1
        ap=456;tricky = Space \\
        Separated \\
        Chaos\;\\
        Head
    ap=789;a=1000;title=Steins\;Gate
cols=3
    group=Variable
        ap=135;tricky \= key = Tricky \= value
        ap=246
    empty tag
    And, yeah, spaces = allowed
    """
    root = VsmlElement.from_string(a)
    b = unicode(root)
    print b
    c = root.as_dict()
    pprint(c, indent=4)