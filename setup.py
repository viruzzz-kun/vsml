# coding=utf-8
import os
try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name='vsml',
    version='0.1',
    url='https://bitbucket.org/viruzzz-kun/vsml',
    author='Mikhael Malkov',
    description='Python very simple markup library',
    long_description=read('README.md'),
    include_package_data=True,
    packages=['vsml'],
    platforms='any',
    classifiers=[
        'Development Status :: 3 - Beta'
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)